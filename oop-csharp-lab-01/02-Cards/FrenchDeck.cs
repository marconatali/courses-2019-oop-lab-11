﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    class FrenchDeck
    {
        private Card[] cards;

        public FrenchDeck()
        {
            cards = new Card[Enum.GetNames(typeof(FrenchSeed)).Length * Enum.GetNames(typeof(FrenchValue)).Length + Enum.GetNames(typeof(Jolly)).Length];
        }

        public Card this[FrenchSeed seed, FrenchValue value]
        {
            get
            {
                int k = 0;
                foreach (FrenchSeed s in (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed)))
                {
                    foreach (FrenchValue v in (FrenchValue[])Enum.GetValues(typeof(FrenchValue)))
                    {
                        if (s.Equals(seed) && v.Equals(value))
                        {
                            return cards[k];
                        }

                        k++;
                    }
                }
                return cards[0];
            }
        }

        public Card this [Jolly jolly]
        {
            get
            {
                int k = 0;
                foreach(Jolly j in (Jolly[])Enum.GetValues(typeof(Jolly)))
                {
                    return cards[k];
                }
                return cards[0];
            }            
        }

        public void Initialize()
        {
            int k = 0;
            foreach (FrenchSeed seed in (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed)))
            {
                foreach (FrenchValue value in (FrenchValue[])Enum.GetValues(typeof(FrenchValue)))
                {
                    cards[k] = new Card(seed.ToString(), value.ToString());
                    k++;
                }
            }
            foreach(Jolly jolly in (Jolly[])Enum.GetValues(typeof(Jolly)))
            {
                cards[k] = new Card(jolly.ToString(), "jolly");
                k++;
            }
        }

        public void Print()
        {
            foreach (Card c in cards)
            {
                Console.WriteLine("Carte del mazzo: \n" + c.ToString());
            }
        }

    }



    enum FrenchSeed
    {
        PICCHE,
        QUADRI,
        FIORI,
        CUORI
    }

    enum FrenchValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        OTTO,
        NOVE,
        DIECI,
        JACK_BONAVENTURA,
        QUEEN,
        KING
    }

    enum Jolly
    {
        RED,
        BLACK
    }
}
