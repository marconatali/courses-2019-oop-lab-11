﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _04_JavaToCSharp.robot.Base;

namespace _04_JavaToCSharp.robot.arms
{
    interface IRobotWithArms : IRobot
    {
        bool PickUp();

        bool DropDown();

        int CarriedItemsCount { get; }
    }
}
