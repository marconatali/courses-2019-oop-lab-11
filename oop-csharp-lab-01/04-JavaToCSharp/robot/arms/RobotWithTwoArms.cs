﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _04_JavaToCSharp.robot.Base;

namespace _04_JavaToCSharp.robot.arms
{
    class RobotWithTwoArms : BaseRobot, IRobotWithArms
    {
        public const double TRANSPORT_OBJECT_CONSUMPTION = 0.1;

        private  BasicArm leftArm;
        private  BasicArm rightArm;

    /**
     * 
     * @param robotName
     * @param batteryLevel
     */
    public RobotWithTwoArms( string robotName) : base(robotName)
        {
            leftArm = new BasicArm("Left arm");
            rightArm = new BasicArm("Right arm");
        }

        protected double BatteryRequirementForMovement
        {
            get { return base.GetBatteryRequirementForMovement() + CarriedItemsCount * TRANSPORT_OBJECT_CONSUMPTION; } 
        }

        public int CarriedItemsCount { get { return (leftArm.IsGrabbing ? 1 : 0) + (rightArm.IsGrabbing ? 1 : 0); } }

        private void DoPick( BasicArm arm)
        {
            if (IsBatteryEnough(arm.ConsuptionForPickUp) && !arm.IsGrabbing)
            {
                Log(arm + " is picking an object");
                arm.PickUp();
                ConsumeBattery(arm.ConsuptionForPickUp);
            }
            else
            {
                Log("Can not grab (battery=" + this.BatteryLevel + "," + arm + " isGrabbing=" + arm.IsGrabbing + ")");
            }
        }

        private void DoRelease( BasicArm arm)
        {
            if (IsBatteryEnough(arm.ConsuptionForDropDown) && arm.IsGrabbing)
            {
                this.Log(arm + " is releasing an object");
                arm.DropDown();
                this.ConsumeBattery(arm.ConsuptionForDropDown);
            }
            else
            {
                Log("Can not release (batteryLevel=" + this.BatteryLevel + "," + arm + " isGrabbing="
                        + arm.IsGrabbing + ")");
            }
        }

        public bool DropDown()
        {
            if (leftArm.IsGrabbing)
            {
                DoRelease(leftArm);
                return true;
            }
            if (rightArm.IsGrabbing)
            {
                DoRelease(rightArm);
                return true;
            }
            return false;
        }

        

        public bool PickUp()
        {
            if (rightArm.IsGrabbing)
            {
                if (leftArm.IsGrabbing)
                {
                    return false;
                }
                DoPick(leftArm);
            }
            else
            {
                DoPick(rightArm);
            }
            return true;
        }
    }
}
