﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.Base
{
    class RobotPosition : IPosition2D
    {
        public int X { get; }


        public int Y { get; }
        public RobotPosition(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int HashCode()
        {
            return X ^ Y;
        }
        
        public IPosition2D SumVector(IPosition2D p)
        {
            return new RobotPosition(X + p.X, Y + p.Y);
        }

        public IPosition2D SumVector(int x, int y)
        {
            return new RobotPosition(X + x, Y + y);
        }

        public override string ToString()
        {
            return "[" + X + ", " + Y + "]";
        }
    }
}
