﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.Base
{
    interface IRobot
    {
        bool MoveUp { get; }

        bool MoveDown { get; }

        bool MoveLeft { get; }

        bool MoveRight { get; }

        /**
         * Fully recharge the robot
         */
        void Recharge();

        /**
         * @return The robot's current battery level
         */
        double BatteryLevel { get; }

        /**
         * @return The robot environment
         */
        IPosition2D Position { get; }
    }
}
