﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.Base
{
    class RobotEnviroment
    {
        /**
    * Environment upper bound for the X coordinate
    */
        public const int X_UPPER_LIMIT = 50;
        /**
         * Environment lower bound for the X coordinate
         */
        public const int X_LOWER_LIMIT = 0;
        /**
         * Environment upper bound for the Y coordinate
         */
        public const int Y_UPPER_LIMIT = 80;
        /**
         * Environment lower bound for the X coordinate
         */
        public const int Y_LOWER_LIMIT = 0;

        public IPosition2D Position { get; set; }

        /**
         * 
         * @param position
         */
        public RobotEnviroment(RobotPosition position)
        {
            Position = position;
        }

        protected bool IsWithinWorld(IPosition2D p)
        {
            var x = p.X;
            var y = p.Y;
            return x >= X_LOWER_LIMIT && x <= X_UPPER_LIMIT && y >= Y_LOWER_LIMIT && y <= Y_UPPER_LIMIT;
        }

        /**
         * Move the robot to a new position
         * 
         * @param newX
         * @param newY
         * @return A boolindicating if the robot moved or not (a robot can move
         *         only inside the environment's boundaries)
         */
        public bool Move(int dx, int dy)
        {
             var newPos = Position.SumVector(dx, dy);
            if (IsWithinWorld(newPos))
            {
                Position = newPos;
                return true;
            }
            return false;
        }
    }
}
