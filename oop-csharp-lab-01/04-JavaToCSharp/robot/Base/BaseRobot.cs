﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.Base
{
    class BaseRobot : IRobot
    {
        public const double BATTERY_FULL = 100;
        public const double MOVEMENT_DELTA_CONSUMPTION = 1.2;
        private const int MOVEMENT_DELTA = 1;

        public double BatteryLevel { get { return Math.Round(BatteryLevel * 100d) / BATTERY_FULL; } set { } }
        private readonly RobotEnviroment environment;
        private readonly string robotName;

        public BaseRobot(string robotName)
        {
            this.environment = new RobotEnviroment(new RobotPosition(0, 0));
            this.robotName = robotName;
            BatteryLevel = BATTERY_FULL;
        }

        protected void ConsumeBattery(double amount)
        {
            if (BatteryLevel >= amount)
            {
                BatteryLevel -= amount;
            }
            else
            {
                BatteryLevel = 0;
            }
        }

        
        private void ConsumeBatteryForMovement()
        {
            ConsumeBattery(GetBatteryRequirementForMovement());
        }

        protected double GetBatteryRequirementForMovement() => MOVEMENT_DELTA_CONSUMPTION;

       
        public IPosition2D Position
        {
            get { return environment.Position; }
        }

        
        protected bool IsBatteryEnough(double operationCost)
        {
            return BatteryLevel > operationCost;
        }

        
        protected void Log( String msg)
        {
            Console.WriteLine("[" + this.robotName + "]: " + msg);
        }

        private bool Move( int dx,  int dy)
        {
            if (IsBatteryEnough(GetBatteryRequirementForMovement()))
            {
                if (environment.Move(dx, dy))
                {
                    ConsumeBatteryForMovement();
                    Log("Moved to position " + environment.Position + ". Battery: " + BatteryLevel + "%.");
                    return true;
                }
                Log("Can not move of (" + dx + "," + dy
                        + ") the robot is touching the world boundary: current position is " + environment.Position);
            }
            else
            {
                Log("Can not move, not enough battery. Required: " + GetBatteryRequirementForMovement()
                    + ", available: " + BatteryLevel + " (" + BatteryLevel + "%)");
            }
            return false;
        }
        
        public bool MoveDown
        {
            get { return Move(0, -MOVEMENT_DELTA); }
        }

       
        public bool MoveLeft
        {
            get { return Move(-MOVEMENT_DELTA, 0); }
        }

       
        public bool MoveRight
        {
            get { return Move(MOVEMENT_DELTA, 0); }
        }

       
        public bool MoveUp
        {
            get { return Move(0, MOVEMENT_DELTA); }
        }

        
        public void Recharge()
        {
            this.BatteryLevel = BATTERY_FULL;
        }

        public override string ToString()
        {
            return robotName;
        }
    }
}
