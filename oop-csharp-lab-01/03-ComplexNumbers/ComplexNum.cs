﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }


        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return Math.Sqrt(Math.Pow(Re,2)+ Math.Pow(Im,2));
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(Re, -Im);
            }
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            return "RE:" + this.Re + "\tIM:" + this.Im;
        }

        public static ComplexNum operator+(ComplexNum n1,ComplexNum n2)
        {
            return new ComplexNum((n1.Re + n2.Re), (n2.Im + n2.Im));
        }

        public static ComplexNum operator -(ComplexNum n1, ComplexNum n2)
        {
            return new ComplexNum((n1.Re - n2.Re), (n2.Im - n2.Im));
        }

        public static ComplexNum operator *(ComplexNum n1, ComplexNum n2)
        {
            double re = n1.Re * n2.Re - n1.Im * n2.Im;
            double im = n1.Re * n2.Im + n2.Re * n1.Im;
            return new ComplexNum(re, im);
        }

        public static ComplexNum operator /(ComplexNum n1, ComplexNum n2)
        {
            double re = (n1.Re * n2.Re + n1.Im * n2.Im) / ((n2.Re * n2.Re) + (n2.Im * n2.Im));
            double im = (n1.Im * n2.Re - n1.Re * n2.Im) / ((n2.Re * n2.Re) + (n2.Im * n2.Im));
            return new ComplexNum(re, im);
        }

        public ComplexNum Invert()
        {
            ComplexNum module = new ComplexNum(Math.Pow(this.Module, 2), 0);
            return (this.Conjugate / module);
        }


    }
}
